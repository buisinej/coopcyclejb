package fr.polytech.info4.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.polytech.info4.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoopnationalDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoopnationalDTO.class);
        CoopnationalDTO coopnationalDTO1 = new CoopnationalDTO();
        coopnationalDTO1.setId("id1");
        CoopnationalDTO coopnationalDTO2 = new CoopnationalDTO();
        assertThat(coopnationalDTO1).isNotEqualTo(coopnationalDTO2);
        coopnationalDTO2.setId(coopnationalDTO1.getId());
        assertThat(coopnationalDTO1).isEqualTo(coopnationalDTO2);
        coopnationalDTO2.setId("id2");
        assertThat(coopnationalDTO1).isNotEqualTo(coopnationalDTO2);
        coopnationalDTO1.setId(null);
        assertThat(coopnationalDTO1).isNotEqualTo(coopnationalDTO2);
    }
}
