package fr.polytech.info4.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import fr.polytech.info4.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CooplocalDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CooplocalDTO.class);
        CooplocalDTO cooplocalDTO1 = new CooplocalDTO();
        cooplocalDTO1.setId("id1");
        CooplocalDTO cooplocalDTO2 = new CooplocalDTO();
        assertThat(cooplocalDTO1).isNotEqualTo(cooplocalDTO2);
        cooplocalDTO2.setId(cooplocalDTO1.getId());
        assertThat(cooplocalDTO1).isEqualTo(cooplocalDTO2);
        cooplocalDTO2.setId("id2");
        assertThat(cooplocalDTO1).isNotEqualTo(cooplocalDTO2);
        cooplocalDTO1.setId(null);
        assertThat(cooplocalDTO1).isNotEqualTo(cooplocalDTO2);
    }
}
