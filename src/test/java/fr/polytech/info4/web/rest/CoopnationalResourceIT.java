package fr.polytech.info4.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.polytech.info4.IntegrationTest;
import fr.polytech.info4.domain.Cooplocal;
import fr.polytech.info4.domain.Coopnational;
import fr.polytech.info4.repository.CoopnationalRepository;
import fr.polytech.info4.service.criteria.CoopnationalCriteria;
import fr.polytech.info4.service.dto.CoopnationalDTO;
import fr.polytech.info4.service.mapper.CoopnationalMapper;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CoopnationalResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CoopnationalResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/coopnationals";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private CoopnationalRepository coopnationalRepository;

    @Autowired
    private CoopnationalMapper coopnationalMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoopnationalMockMvc;

    private Coopnational coopnational;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coopnational createEntity(EntityManager em) {
        Coopnational coopnational = new Coopnational().name(DEFAULT_NAME);
        return coopnational;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coopnational createUpdatedEntity(EntityManager em) {
        Coopnational coopnational = new Coopnational().name(UPDATED_NAME);
        return coopnational;
    }

    @BeforeEach
    public void initTest() {
        coopnational = createEntity(em);
    }

    @Test
    @Transactional
    void createCoopnational() throws Exception {
        int databaseSizeBeforeCreate = coopnationalRepository.findAll().size();
        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);
        restCoopnationalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeCreate + 1);
        Coopnational testCoopnational = coopnationalList.get(coopnationalList.size() - 1);
        assertThat(testCoopnational.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createCoopnationalWithExistingId() throws Exception {
        // Create the Coopnational with an existing ID
        coopnational.setId("existing_id");
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        int databaseSizeBeforeCreate = coopnationalRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoopnationalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = coopnationalRepository.findAll().size();
        // set the field null
        coopnational.setName(null);

        // Create the Coopnational, which fails.
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        restCoopnationalMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCoopnationals() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coopnational.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getCoopnational() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get the coopnational
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL_ID, coopnational.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coopnational.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getCoopnationalsByIdFiltering() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        String id = coopnational.getId();

        defaultCoopnationalShouldBeFound("id.equals=" + id);
        defaultCoopnationalShouldNotBeFound("id.notEquals=" + id);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name equals to DEFAULT_NAME
        defaultCoopnationalShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the coopnationalList where name equals to UPDATED_NAME
        defaultCoopnationalShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name not equals to DEFAULT_NAME
        defaultCoopnationalShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the coopnationalList where name not equals to UPDATED_NAME
        defaultCoopnationalShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCoopnationalShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the coopnationalList where name equals to UPDATED_NAME
        defaultCoopnationalShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name is not null
        defaultCoopnationalShouldBeFound("name.specified=true");

        // Get all the coopnationalList where name is null
        defaultCoopnationalShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameContainsSomething() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name contains DEFAULT_NAME
        defaultCoopnationalShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the coopnationalList where name contains UPDATED_NAME
        defaultCoopnationalShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        // Get all the coopnationalList where name does not contain DEFAULT_NAME
        defaultCoopnationalShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the coopnationalList where name does not contain UPDATED_NAME
        defaultCoopnationalShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCoopnationalsByCooplocalIsEqualToSomething() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);
        Cooplocal cooplocal = CooplocalResourceIT.createEntity(em);
        em.persist(cooplocal);
        em.flush();
        coopnational.setCooplocal(cooplocal);
        coopnationalRepository.saveAndFlush(coopnational);
        String cooplocalId = cooplocal.getId();

        // Get all the coopnationalList where cooplocal equals to cooplocalId
        defaultCoopnationalShouldBeFound("cooplocalId.equals=" + cooplocalId);

        // Get all the coopnationalList where cooplocal equals to "invalid-id"
        defaultCoopnationalShouldNotBeFound("cooplocalId.equals=" + "invalid-id");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCoopnationalShouldBeFound(String filter) throws Exception {
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coopnational.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCoopnationalShouldNotBeFound(String filter) throws Exception {
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCoopnationalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCoopnational() throws Exception {
        // Get the coopnational
        restCoopnationalMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCoopnational() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();

        // Update the coopnational
        Coopnational updatedCoopnational = coopnationalRepository.findById(coopnational.getId()).get();
        // Disconnect from session so that the updates on updatedCoopnational are not directly saved in db
        em.detach(updatedCoopnational);
        updatedCoopnational.name(UPDATED_NAME);
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(updatedCoopnational);

        restCoopnationalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coopnationalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isOk());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
        Coopnational testCoopnational = coopnationalList.get(coopnationalList.size() - 1);
        assertThat(testCoopnational.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coopnationalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoopnationalWithPatch() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();

        // Update the coopnational using partial update
        Coopnational partialUpdatedCoopnational = new Coopnational();
        partialUpdatedCoopnational.setId(coopnational.getId());

        partialUpdatedCoopnational.name(UPDATED_NAME);

        restCoopnationalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoopnational.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoopnational))
            )
            .andExpect(status().isOk());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
        Coopnational testCoopnational = coopnationalList.get(coopnationalList.size() - 1);
        assertThat(testCoopnational.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void fullUpdateCoopnationalWithPatch() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();

        // Update the coopnational using partial update
        Coopnational partialUpdatedCoopnational = new Coopnational();
        partialUpdatedCoopnational.setId(coopnational.getId());

        partialUpdatedCoopnational.name(UPDATED_NAME);

        restCoopnationalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoopnational.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoopnational))
            )
            .andExpect(status().isOk());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
        Coopnational testCoopnational = coopnationalList.get(coopnationalList.size() - 1);
        assertThat(testCoopnational.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coopnationalDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoopnational() throws Exception {
        int databaseSizeBeforeUpdate = coopnationalRepository.findAll().size();
        coopnational.setId(UUID.randomUUID().toString());

        // Create the Coopnational
        CoopnationalDTO coopnationalDTO = coopnationalMapper.toDto(coopnational);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoopnationalMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coopnationalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coopnational in the database
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoopnational() throws Exception {
        // Initialize the database
        coopnationalRepository.saveAndFlush(coopnational);

        int databaseSizeBeforeDelete = coopnationalRepository.findAll().size();

        // Delete the coopnational
        restCoopnationalMockMvc
            .perform(delete(ENTITY_API_URL_ID, coopnational.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coopnational> coopnationalList = coopnationalRepository.findAll();
        assertThat(coopnationalList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
