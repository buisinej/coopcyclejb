package fr.polytech.info4.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.polytech.info4.IntegrationTest;
import fr.polytech.info4.domain.Cooplocal;
import fr.polytech.info4.domain.Coopnational;
import fr.polytech.info4.domain.Livreur;
import fr.polytech.info4.domain.Restaurant;
import fr.polytech.info4.repository.CooplocalRepository;
import fr.polytech.info4.service.criteria.CooplocalCriteria;
import fr.polytech.info4.service.dto.CooplocalDTO;
import fr.polytech.info4.service.mapper.CooplocalMapper;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CooplocalResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CooplocalResourceIT {

    private static final String ENTITY_API_URL = "/api/cooplocals";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private CooplocalRepository cooplocalRepository;

    @Autowired
    private CooplocalMapper cooplocalMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCooplocalMockMvc;

    private Cooplocal cooplocal;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cooplocal createEntity(EntityManager em) {
        Cooplocal cooplocal = new Cooplocal();
        return cooplocal;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cooplocal createUpdatedEntity(EntityManager em) {
        Cooplocal cooplocal = new Cooplocal();
        return cooplocal;
    }

    @BeforeEach
    public void initTest() {
        cooplocal = createEntity(em);
    }

    @Test
    @Transactional
    void createCooplocal() throws Exception {
        int databaseSizeBeforeCreate = cooplocalRepository.findAll().size();
        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);
        restCooplocalMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cooplocalDTO)))
            .andExpect(status().isCreated());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeCreate + 1);
        Cooplocal testCooplocal = cooplocalList.get(cooplocalList.size() - 1);
    }

    @Test
    @Transactional
    void createCooplocalWithExistingId() throws Exception {
        // Create the Cooplocal with an existing ID
        cooplocal.setId("existing_id");
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        int databaseSizeBeforeCreate = cooplocalRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCooplocalMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cooplocalDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCooplocals() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        // Get all the cooplocalList
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cooplocal.getId())));
    }

    @Test
    @Transactional
    void getCooplocal() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        // Get the cooplocal
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL_ID, cooplocal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(cooplocal.getId()));
    }

    @Test
    @Transactional
    void getCooplocalsByIdFiltering() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        String id = cooplocal.getId();

        defaultCooplocalShouldBeFound("id.equals=" + id);
        defaultCooplocalShouldNotBeFound("id.notEquals=" + id);
    }

    @Test
    @Transactional
    void getAllCooplocalsByCoopnationalIsEqualToSomething() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);
        Coopnational coopnational = CoopnationalResourceIT.createEntity(em);
        em.persist(coopnational);
        em.flush();
        cooplocal.addCoopnational(coopnational);
        cooplocalRepository.saveAndFlush(cooplocal);
        String coopnationalId = coopnational.getId();

        // Get all the cooplocalList where coopnational equals to coopnationalId
        defaultCooplocalShouldBeFound("coopnationalId.equals=" + coopnationalId);

        // Get all the cooplocalList where coopnational equals to "invalid-id"
        defaultCooplocalShouldNotBeFound("coopnationalId.equals=" + "invalid-id");
    }

    @Test
    @Transactional
    void getAllCooplocalsByLivreurIsEqualToSomething() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);
        Livreur livreur = LivreurResourceIT.createEntity(em);
        em.persist(livreur);
        em.flush();
        cooplocal.addLivreur(livreur);
        cooplocalRepository.saveAndFlush(cooplocal);
        Long livreurId = livreur.getId();

        // Get all the cooplocalList where livreur equals to livreurId
        defaultCooplocalShouldBeFound("livreurId.equals=" + livreurId);

        // Get all the cooplocalList where livreur equals to (livreurId + 1)
        defaultCooplocalShouldNotBeFound("livreurId.equals=" + (livreurId + 1));
    }

    @Test
    @Transactional
    void getAllCooplocalsByRestaurantIsEqualToSomething() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);
        Restaurant restaurant = RestaurantResourceIT.createEntity(em);
        em.persist(restaurant);
        em.flush();
        cooplocal.addRestaurant(restaurant);
        cooplocalRepository.saveAndFlush(cooplocal);
        Long restaurantId = restaurant.getId();

        // Get all the cooplocalList where restaurant equals to restaurantId
        defaultCooplocalShouldBeFound("restaurantId.equals=" + restaurantId);

        // Get all the cooplocalList where restaurant equals to (restaurantId + 1)
        defaultCooplocalShouldNotBeFound("restaurantId.equals=" + (restaurantId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCooplocalShouldBeFound(String filter) throws Exception {
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cooplocal.getId())));

        // Check, that the count call also returns 1
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCooplocalShouldNotBeFound(String filter) throws Exception {
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCooplocalMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCooplocal() throws Exception {
        // Get the cooplocal
        restCooplocalMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCooplocal() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();

        // Update the cooplocal
        Cooplocal updatedCooplocal = cooplocalRepository.findById(cooplocal.getId()).get();
        // Disconnect from session so that the updates on updatedCooplocal are not directly saved in db
        em.detach(updatedCooplocal);
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(updatedCooplocal);

        restCooplocalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cooplocalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isOk());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
        Cooplocal testCooplocal = cooplocalList.get(cooplocalList.size() - 1);
    }

    @Test
    @Transactional
    void putNonExistingCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, cooplocalDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(
                put(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(cooplocalDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCooplocalWithPatch() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();

        // Update the cooplocal using partial update
        Cooplocal partialUpdatedCooplocal = new Cooplocal();
        partialUpdatedCooplocal.setId(cooplocal.getId());

        restCooplocalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCooplocal.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCooplocal))
            )
            .andExpect(status().isOk());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
        Cooplocal testCooplocal = cooplocalList.get(cooplocalList.size() - 1);
    }

    @Test
    @Transactional
    void fullUpdateCooplocalWithPatch() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();

        // Update the cooplocal using partial update
        Cooplocal partialUpdatedCooplocal = new Cooplocal();
        partialUpdatedCooplocal.setId(cooplocal.getId());

        restCooplocalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCooplocal.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCooplocal))
            )
            .andExpect(status().isOk());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
        Cooplocal testCooplocal = cooplocalList.get(cooplocalList.size() - 1);
    }

    @Test
    @Transactional
    void patchNonExistingCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, cooplocalDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, UUID.randomUUID().toString())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCooplocal() throws Exception {
        int databaseSizeBeforeUpdate = cooplocalRepository.findAll().size();
        cooplocal.setId(UUID.randomUUID().toString());

        // Create the Cooplocal
        CooplocalDTO cooplocalDTO = cooplocalMapper.toDto(cooplocal);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCooplocalMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(cooplocalDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Cooplocal in the database
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCooplocal() throws Exception {
        // Initialize the database
        cooplocalRepository.saveAndFlush(cooplocal);

        int databaseSizeBeforeDelete = cooplocalRepository.findAll().size();

        // Delete the cooplocal
        restCooplocalMockMvc
            .perform(delete(ENTITY_API_URL_ID, cooplocal.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Cooplocal> cooplocalList = cooplocalRepository.findAll();
        assertThat(cooplocalList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
