import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { CooplocalComponentsPage, CooplocalDeleteDialog, CooplocalUpdatePage } from './cooplocal.page-object';

const expect = chai.expect;

describe('Cooplocal e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let cooplocalComponentsPage: CooplocalComponentsPage;
  let cooplocalUpdatePage: CooplocalUpdatePage;
  let cooplocalDeleteDialog: CooplocalDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Cooplocals', async () => {
    await navBarPage.goToEntity('cooplocal');
    cooplocalComponentsPage = new CooplocalComponentsPage();
    await browser.wait(ec.visibilityOf(cooplocalComponentsPage.title), 5000);
    expect(await cooplocalComponentsPage.getTitle()).to.eq('myblogApp.cooplocal.home.title');
    await browser.wait(ec.or(ec.visibilityOf(cooplocalComponentsPage.entities), ec.visibilityOf(cooplocalComponentsPage.noResult)), 1000);
  });

  it('should load create Cooplocal page', async () => {
    await cooplocalComponentsPage.clickOnCreateButton();
    cooplocalUpdatePage = new CooplocalUpdatePage();
    expect(await cooplocalUpdatePage.getPageTitle()).to.eq('myblogApp.cooplocal.home.createOrEditLabel');
    await cooplocalUpdatePage.cancel();
  });

  it('should create and save Cooplocals', async () => {
    const nbButtonsBeforeCreate = await cooplocalComponentsPage.countDeleteButtons();

    await cooplocalComponentsPage.clickOnCreateButton();

    await promise.all([]);

    await cooplocalUpdatePage.save();
    expect(await cooplocalUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await cooplocalComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Cooplocal', async () => {
    const nbButtonsBeforeDelete = await cooplocalComponentsPage.countDeleteButtons();
    await cooplocalComponentsPage.clickOnLastDeleteButton();

    cooplocalDeleteDialog = new CooplocalDeleteDialog();
    expect(await cooplocalDeleteDialog.getDialogTitle()).to.eq('myblogApp.cooplocal.delete.question');
    await cooplocalDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(cooplocalComponentsPage.title), 5000);

    expect(await cooplocalComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
