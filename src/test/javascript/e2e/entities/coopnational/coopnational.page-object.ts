import { element, by, ElementFinder } from 'protractor';

export class CoopnationalComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-coopnational div table .btn-danger'));
  title = element.all(by.css('jhi-coopnational div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CoopnationalUpdatePage {
  pageTitle = element(by.id('jhi-coopnational-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  nameInput = element(by.id('field_name'));

  cooplocalSelect = element(by.id('field_cooplocal'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async cooplocalSelectLastOption(): Promise<void> {
    await this.cooplocalSelect.all(by.tagName('option')).last().click();
  }

  async cooplocalSelectOption(option: string): Promise<void> {
    await this.cooplocalSelect.sendKeys(option);
  }

  getCooplocalSelect(): ElementFinder {
    return this.cooplocalSelect;
  }

  async getCooplocalSelectedOption(): Promise<string> {
    return await this.cooplocalSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CoopnationalDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-coopnational-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-coopnational'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
