import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PlatComponentsPage, PlatDeleteDialog, PlatUpdatePage } from './plat.page-object';

const expect = chai.expect;

describe('Plat e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let platComponentsPage: PlatComponentsPage;
  let platUpdatePage: PlatUpdatePage;
  let platDeleteDialog: PlatDeleteDialog;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing(username, password);
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Plats', async () => {
    await navBarPage.goToEntity('plat');
    platComponentsPage = new PlatComponentsPage();
    await browser.wait(ec.visibilityOf(platComponentsPage.title), 5000);
    expect(await platComponentsPage.getTitle()).to.eq('myblogApp.plat.home.title');
    await browser.wait(ec.or(ec.visibilityOf(platComponentsPage.entities), ec.visibilityOf(platComponentsPage.noResult)), 1000);
  });

  it('should load create Plat page', async () => {
    await platComponentsPage.clickOnCreateButton();
    platUpdatePage = new PlatUpdatePage();
    expect(await platUpdatePage.getPageTitle()).to.eq('myblogApp.plat.home.createOrEditLabel');
    await platUpdatePage.cancel();
  });

  it('should create and save Plats', async () => {
    const nbButtonsBeforeCreate = await platComponentsPage.countDeleteButtons();

    await platComponentsPage.clickOnCreateButton();

    await promise.all([platUpdatePage.setNameInput('name'), platUpdatePage.setPrixInput('5'), platUpdatePage.restaurantSelectLastOption()]);

    expect(await platUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await platUpdatePage.getPrixInput()).to.eq('5', 'Expected prix value to be equals to 5');

    await platUpdatePage.save();
    expect(await platUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await platComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Plat', async () => {
    const nbButtonsBeforeDelete = await platComponentsPage.countDeleteButtons();
    await platComponentsPage.clickOnLastDeleteButton();

    platDeleteDialog = new PlatDeleteDialog();
    expect(await platDeleteDialog.getDialogTitle()).to.eq('myblogApp.plat.delete.question');
    await platDeleteDialog.clickOnConfirmButton();
    await browser.wait(ec.visibilityOf(platComponentsPage.title), 5000);

    expect(await platComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
