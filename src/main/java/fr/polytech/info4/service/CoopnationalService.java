package fr.polytech.info4.service;

import fr.polytech.info4.service.dto.CoopnationalDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.polytech.info4.domain.Coopnational}.
 */
public interface CoopnationalService {
    /**
     * Save a coopnational.
     *
     * @param coopnationalDTO the entity to save.
     * @return the persisted entity.
     */
    CoopnationalDTO save(CoopnationalDTO coopnationalDTO);

    /**
     * Partially updates a coopnational.
     *
     * @param coopnationalDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CoopnationalDTO> partialUpdate(CoopnationalDTO coopnationalDTO);

    /**
     * Get all the coopnationals.
     *
     * @return the list of entities.
     */
    List<CoopnationalDTO> findAll();

    /**
     * Get the "id" coopnational.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CoopnationalDTO> findOne(String id);

    /**
     * Delete the "id" coopnational.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
