package fr.polytech.info4.service;

import fr.polytech.info4.domain.*; // for static metamodels
import fr.polytech.info4.domain.Coopnational;
import fr.polytech.info4.repository.CoopnationalRepository;
import fr.polytech.info4.service.criteria.CoopnationalCriteria;
import fr.polytech.info4.service.dto.CoopnationalDTO;
import fr.polytech.info4.service.mapper.CoopnationalMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Coopnational} entities in the database.
 * The main input is a {@link CoopnationalCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CoopnationalDTO} or a {@link Page} of {@link CoopnationalDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CoopnationalQueryService extends QueryService<Coopnational> {

    private final Logger log = LoggerFactory.getLogger(CoopnationalQueryService.class);

    private final CoopnationalRepository coopnationalRepository;

    private final CoopnationalMapper coopnationalMapper;

    public CoopnationalQueryService(CoopnationalRepository coopnationalRepository, CoopnationalMapper coopnationalMapper) {
        this.coopnationalRepository = coopnationalRepository;
        this.coopnationalMapper = coopnationalMapper;
    }

    /**
     * Return a {@link List} of {@link CoopnationalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CoopnationalDTO> findByCriteria(CoopnationalCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Coopnational> specification = createSpecification(criteria);
        return coopnationalMapper.toDto(coopnationalRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CoopnationalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CoopnationalDTO> findByCriteria(CoopnationalCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Coopnational> specification = createSpecification(criteria);
        return coopnationalRepository.findAll(specification, page).map(coopnationalMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CoopnationalCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Coopnational> specification = createSpecification(criteria);
        return coopnationalRepository.count(specification);
    }

    /**
     * Function to convert {@link CoopnationalCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Coopnational> createSpecification(CoopnationalCriteria criteria) {
        Specification<Coopnational> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getId(), Coopnational_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Coopnational_.name));
            }
            if (criteria.getCooplocalId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCooplocalId(),
                            root -> root.join(Coopnational_.cooplocal, JoinType.LEFT).get(Cooplocal_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
