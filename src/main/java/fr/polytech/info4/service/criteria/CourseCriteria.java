package fr.polytech.info4.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Course} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.CourseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /courses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CourseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter prix;

    private FloatFilter distance;

    private LongFilter commandeId;

    private LongFilter livreurId;

    public CourseCriteria() {}

    public CourseCriteria(CourseCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.prix = other.prix == null ? null : other.prix.copy();
        this.distance = other.distance == null ? null : other.distance.copy();
        this.commandeId = other.commandeId == null ? null : other.commandeId.copy();
        this.livreurId = other.livreurId == null ? null : other.livreurId.copy();
    }

    @Override
    public CourseCriteria copy() {
        return new CourseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getPrix() {
        return prix;
    }

    public IntegerFilter prix() {
        if (prix == null) {
            prix = new IntegerFilter();
        }
        return prix;
    }

    public void setPrix(IntegerFilter prix) {
        this.prix = prix;
    }

    public FloatFilter getDistance() {
        return distance;
    }

    public FloatFilter distance() {
        if (distance == null) {
            distance = new FloatFilter();
        }
        return distance;
    }

    public void setDistance(FloatFilter distance) {
        this.distance = distance;
    }

    public LongFilter getCommandeId() {
        return commandeId;
    }

    public LongFilter commandeId() {
        if (commandeId == null) {
            commandeId = new LongFilter();
        }
        return commandeId;
    }

    public void setCommandeId(LongFilter commandeId) {
        this.commandeId = commandeId;
    }

    public LongFilter getLivreurId() {
        return livreurId;
    }

    public LongFilter livreurId() {
        if (livreurId == null) {
            livreurId = new LongFilter();
        }
        return livreurId;
    }

    public void setLivreurId(LongFilter livreurId) {
        this.livreurId = livreurId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CourseCriteria that = (CourseCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(prix, that.prix) &&
            Objects.equals(distance, that.distance) &&
            Objects.equals(commandeId, that.commandeId) &&
            Objects.equals(livreurId, that.livreurId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prix, distance, commandeId, livreurId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (prix != null ? "prix=" + prix + ", " : "") +
            (distance != null ? "distance=" + distance + ", " : "") +
            (commandeId != null ? "commandeId=" + commandeId + ", " : "") +
            (livreurId != null ? "livreurId=" + livreurId + ", " : "") +
            "}";
    }
}
