package fr.polytech.info4.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Livreur} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.LivreurResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /livreurs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class LivreurCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LongFilter courseId;

    private StringFilter cooplocalId;

    public LivreurCriteria() {}

    public LivreurCriteria(LivreurCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.courseId = other.courseId == null ? null : other.courseId.copy();
        this.cooplocalId = other.cooplocalId == null ? null : other.cooplocalId.copy();
    }

    @Override
    public LivreurCriteria copy() {
        return new LivreurCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LongFilter getCourseId() {
        return courseId;
    }

    public LongFilter courseId() {
        if (courseId == null) {
            courseId = new LongFilter();
        }
        return courseId;
    }

    public void setCourseId(LongFilter courseId) {
        this.courseId = courseId;
    }

    public StringFilter getCooplocalId() {
        return cooplocalId;
    }

    public StringFilter cooplocalId() {
        if (cooplocalId == null) {
            cooplocalId = new StringFilter();
        }
        return cooplocalId;
    }

    public void setCooplocalId(StringFilter cooplocalId) {
        this.cooplocalId = cooplocalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final LivreurCriteria that = (LivreurCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(courseId, that.courseId) &&
            Objects.equals(cooplocalId, that.cooplocalId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, courseId, cooplocalId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LivreurCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (courseId != null ? "courseId=" + courseId + ", " : "") +
            (cooplocalId != null ? "cooplocalId=" + cooplocalId + ", " : "") +
            "}";
    }
}
