package fr.polytech.info4.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link fr.polytech.info4.domain.Cooplocal} entity. This class is used
 * in {@link fr.polytech.info4.web.rest.CooplocalResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cooplocals?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CooplocalCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private StringFilter id;

    private StringFilter coopnationalId;

    private LongFilter livreurId;

    private LongFilter restaurantId;

    public CooplocalCriteria() {}

    public CooplocalCriteria(CooplocalCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.coopnationalId = other.coopnationalId == null ? null : other.coopnationalId.copy();
        this.livreurId = other.livreurId == null ? null : other.livreurId.copy();
        this.restaurantId = other.restaurantId == null ? null : other.restaurantId.copy();
    }

    @Override
    public CooplocalCriteria copy() {
        return new CooplocalCriteria(this);
    }

    public StringFilter getId() {
        return id;
    }

    public StringFilter id() {
        if (id == null) {
            id = new StringFilter();
        }
        return id;
    }

    public void setId(StringFilter id) {
        this.id = id;
    }

    public StringFilter getCoopnationalId() {
        return coopnationalId;
    }

    public StringFilter coopnationalId() {
        if (coopnationalId == null) {
            coopnationalId = new StringFilter();
        }
        return coopnationalId;
    }

    public void setCoopnationalId(StringFilter coopnationalId) {
        this.coopnationalId = coopnationalId;
    }

    public LongFilter getLivreurId() {
        return livreurId;
    }

    public LongFilter livreurId() {
        if (livreurId == null) {
            livreurId = new LongFilter();
        }
        return livreurId;
    }

    public void setLivreurId(LongFilter livreurId) {
        this.livreurId = livreurId;
    }

    public LongFilter getRestaurantId() {
        return restaurantId;
    }

    public LongFilter restaurantId() {
        if (restaurantId == null) {
            restaurantId = new LongFilter();
        }
        return restaurantId;
    }

    public void setRestaurantId(LongFilter restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CooplocalCriteria that = (CooplocalCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(coopnationalId, that.coopnationalId) &&
            Objects.equals(livreurId, that.livreurId) &&
            Objects.equals(restaurantId, that.restaurantId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, coopnationalId, livreurId, restaurantId);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooplocalCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (coopnationalId != null ? "coopnationalId=" + coopnationalId + ", " : "") +
            (livreurId != null ? "livreurId=" + livreurId + ", " : "") +
            (restaurantId != null ? "restaurantId=" + restaurantId + ", " : "") +
            "}";
    }
}
