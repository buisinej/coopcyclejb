package fr.polytech.info4.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link fr.polytech.info4.domain.Cooplocal} entity.
 */
public class CooplocalDTO implements Serializable {

    @NotNull
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CooplocalDTO)) {
            return false;
        }

        CooplocalDTO cooplocalDTO = (CooplocalDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cooplocalDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooplocalDTO{" +
            "id='" + getId() + "'" +
            "}";
    }
}
