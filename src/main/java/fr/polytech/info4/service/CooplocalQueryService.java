package fr.polytech.info4.service;

import fr.polytech.info4.domain.*; // for static metamodels
import fr.polytech.info4.domain.Cooplocal;
import fr.polytech.info4.repository.CooplocalRepository;
import fr.polytech.info4.service.criteria.CooplocalCriteria;
import fr.polytech.info4.service.dto.CooplocalDTO;
import fr.polytech.info4.service.mapper.CooplocalMapper;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Cooplocal} entities in the database.
 * The main input is a {@link CooplocalCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CooplocalDTO} or a {@link Page} of {@link CooplocalDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CooplocalQueryService extends QueryService<Cooplocal> {

    private final Logger log = LoggerFactory.getLogger(CooplocalQueryService.class);

    private final CooplocalRepository cooplocalRepository;

    private final CooplocalMapper cooplocalMapper;

    public CooplocalQueryService(CooplocalRepository cooplocalRepository, CooplocalMapper cooplocalMapper) {
        this.cooplocalRepository = cooplocalRepository;
        this.cooplocalMapper = cooplocalMapper;
    }

    /**
     * Return a {@link List} of {@link CooplocalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CooplocalDTO> findByCriteria(CooplocalCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cooplocal> specification = createSpecification(criteria);
        return cooplocalMapper.toDto(cooplocalRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CooplocalDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CooplocalDTO> findByCriteria(CooplocalCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cooplocal> specification = createSpecification(criteria);
        return cooplocalRepository.findAll(specification, page).map(cooplocalMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CooplocalCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cooplocal> specification = createSpecification(criteria);
        return cooplocalRepository.count(specification);
    }

    /**
     * Function to convert {@link CooplocalCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Cooplocal> createSpecification(CooplocalCriteria criteria) {
        Specification<Cooplocal> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getId(), Cooplocal_.id));
            }
            if (criteria.getCoopnationalId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCoopnationalId(),
                            root -> root.join(Cooplocal_.coopnationals, JoinType.LEFT).get(Coopnational_.id)
                        )
                    );
            }
            if (criteria.getLivreurId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getLivreurId(), root -> root.join(Cooplocal_.livreurs, JoinType.LEFT).get(Livreur_.id))
                    );
            }
            if (criteria.getRestaurantId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getRestaurantId(),
                            root -> root.join(Cooplocal_.restaurants, JoinType.LEFT).get(Restaurant_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
