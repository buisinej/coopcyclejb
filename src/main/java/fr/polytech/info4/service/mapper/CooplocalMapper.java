package fr.polytech.info4.service.mapper;

import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.CooplocalDTO;
import java.util.Set;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cooplocal} and its DTO {@link CooplocalDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CooplocalMapper extends EntityMapper<CooplocalDTO, Cooplocal> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooplocalDTO toDtoId(Cooplocal cooplocal);

    @Named("idSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    Set<CooplocalDTO> toDtoIdSet(Set<Cooplocal> cooplocal);
}
