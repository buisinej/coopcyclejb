package fr.polytech.info4.service.mapper;

import fr.polytech.info4.domain.*;
import fr.polytech.info4.service.dto.CommandeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commande} and its DTO {@link CommandeDTO}.
 */
@Mapper(componentModel = "spring", uses = { ClientMapper.class, CourseMapper.class, RestaurantMapper.class, PlatMapper.class })
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {
    @Mapping(target = "client", source = "client", qualifiedByName = "id")
    @Mapping(target = "course", source = "course", qualifiedByName = "id")
    @Mapping(target = "restaurant", source = "restaurant", qualifiedByName = "id")
    @Mapping(target = "plat", source = "plat", qualifiedByName = "id")
    CommandeDTO toDto(Commande s);
}
