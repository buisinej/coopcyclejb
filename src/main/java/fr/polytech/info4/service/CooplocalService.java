package fr.polytech.info4.service;

import fr.polytech.info4.service.dto.CooplocalDTO;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link fr.polytech.info4.domain.Cooplocal}.
 */
public interface CooplocalService {
    /**
     * Save a cooplocal.
     *
     * @param cooplocalDTO the entity to save.
     * @return the persisted entity.
     */
    CooplocalDTO save(CooplocalDTO cooplocalDTO);

    /**
     * Partially updates a cooplocal.
     *
     * @param cooplocalDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CooplocalDTO> partialUpdate(CooplocalDTO cooplocalDTO);

    /**
     * Get all the cooplocals.
     *
     * @return the list of entities.
     */
    List<CooplocalDTO> findAll();

    /**
     * Get the "id" cooplocal.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CooplocalDTO> findOne(String id);

    /**
     * Delete the "id" cooplocal.
     *
     * @param id the id of the entity.
     */
    void delete(String id);
}
