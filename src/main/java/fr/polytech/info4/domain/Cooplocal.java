package fr.polytech.info4.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Cooplocal.
 */
@Entity
@Table(name = "cooplocal")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Cooplocal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @OneToMany(mappedBy = "cooplocal")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "cooplocal" }, allowSetters = true)
    private Set<Coopnational> coopnationals = new HashSet<>();

    @OneToMany(mappedBy = "cooplocal")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "courses", "cooplocal" }, allowSetters = true)
    private Set<Livreur> livreurs = new HashSet<>();

    @ManyToMany(mappedBy = "cooplocals")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "plats", "commandes", "cooplocals" }, allowSetters = true)
    private Set<Restaurant> restaurants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cooplocal id(String id) {
        this.id = id;
        return this;
    }

    public Set<Coopnational> getCoopnationals() {
        return this.coopnationals;
    }

    public Cooplocal coopnationals(Set<Coopnational> coopnationals) {
        this.setCoopnationals(coopnationals);
        return this;
    }

    public Cooplocal addCoopnational(Coopnational coopnational) {
        this.coopnationals.add(coopnational);
        coopnational.setCooplocal(this);
        return this;
    }

    public Cooplocal removeCoopnational(Coopnational coopnational) {
        this.coopnationals.remove(coopnational);
        coopnational.setCooplocal(null);
        return this;
    }

    public void setCoopnationals(Set<Coopnational> coopnationals) {
        if (this.coopnationals != null) {
            this.coopnationals.forEach(i -> i.setCooplocal(null));
        }
        if (coopnationals != null) {
            coopnationals.forEach(i -> i.setCooplocal(this));
        }
        this.coopnationals = coopnationals;
    }

    public Set<Livreur> getLivreurs() {
        return this.livreurs;
    }

    public Cooplocal livreurs(Set<Livreur> livreurs) {
        this.setLivreurs(livreurs);
        return this;
    }

    public Cooplocal addLivreur(Livreur livreur) {
        this.livreurs.add(livreur);
        livreur.setCooplocal(this);
        return this;
    }

    public Cooplocal removeLivreur(Livreur livreur) {
        this.livreurs.remove(livreur);
        livreur.setCooplocal(null);
        return this;
    }

    public void setLivreurs(Set<Livreur> livreurs) {
        if (this.livreurs != null) {
            this.livreurs.forEach(i -> i.setCooplocal(null));
        }
        if (livreurs != null) {
            livreurs.forEach(i -> i.setCooplocal(this));
        }
        this.livreurs = livreurs;
    }

    public Set<Restaurant> getRestaurants() {
        return this.restaurants;
    }

    public Cooplocal restaurants(Set<Restaurant> restaurants) {
        this.setRestaurants(restaurants);
        return this;
    }

    public Cooplocal addRestaurant(Restaurant restaurant) {
        this.restaurants.add(restaurant);
        restaurant.getCooplocals().add(this);
        return this;
    }

    public Cooplocal removeRestaurant(Restaurant restaurant) {
        this.restaurants.remove(restaurant);
        restaurant.getCooplocals().remove(this);
        return this;
    }

    public void setRestaurants(Set<Restaurant> restaurants) {
        if (this.restaurants != null) {
            this.restaurants.forEach(i -> i.removeCooplocal(this));
        }
        if (restaurants != null) {
            restaurants.forEach(i -> i.addCooplocal(this));
        }
        this.restaurants = restaurants;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cooplocal)) {
            return false;
        }
        return id != null && id.equals(((Cooplocal) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cooplocal{" +
            "id=" + getId() +
            "}";
    }
}
