package fr.polytech.info4.repository;

import fr.polytech.info4.domain.Cooplocal;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Cooplocal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CooplocalRepository extends JpaRepository<Cooplocal, String>, JpaSpecificationExecutor<Cooplocal> {}
