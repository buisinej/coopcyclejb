package fr.polytech.info4.repository;

import fr.polytech.info4.domain.Coopnational;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Coopnational entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoopnationalRepository extends JpaRepository<Coopnational, String>, JpaSpecificationExecutor<Coopnational> {}
