jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { ICooplocal, Cooplocal } from '../cooplocal.model';
import { CooplocalService } from '../service/cooplocal.service';

import { CooplocalRoutingResolveService } from './cooplocal-routing-resolve.service';

describe('Service Tests', () => {
  describe('Cooplocal routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: CooplocalRoutingResolveService;
    let service: CooplocalService;
    let resultCooplocal: ICooplocal | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(CooplocalRoutingResolveService);
      service = TestBed.inject(CooplocalService);
      resultCooplocal = undefined;
    });

    describe('resolve', () => {
      it('should return ICooplocal returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCooplocal = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultCooplocal).toEqual({ id: 'ABC' });
      });

      it('should return new ICooplocal if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCooplocal = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultCooplocal).toEqual(new Cooplocal());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 'ABC' };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultCooplocal = result;
        });

        // THEN
        expect(service.find).toBeCalledWith('ABC');
        expect(resultCooplocal).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
