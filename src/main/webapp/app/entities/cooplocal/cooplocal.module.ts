import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { CooplocalComponent } from './list/cooplocal.component';
import { CooplocalDetailComponent } from './detail/cooplocal-detail.component';
import { CooplocalUpdateComponent } from './update/cooplocal-update.component';
import { CooplocalDeleteDialogComponent } from './delete/cooplocal-delete-dialog.component';
import { CooplocalRoutingModule } from './route/cooplocal-routing.module';

@NgModule({
  imports: [SharedModule, CooplocalRoutingModule],
  declarations: [CooplocalComponent, CooplocalDetailComponent, CooplocalUpdateComponent, CooplocalDeleteDialogComponent],
  entryComponents: [CooplocalDeleteDialogComponent],
})
export class CooplocalModule {}
