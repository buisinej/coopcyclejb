import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICooplocal, Cooplocal } from '../cooplocal.model';
import { CooplocalService } from '../service/cooplocal.service';

@Component({
  selector: 'jhi-cooplocal-update',
  templateUrl: './cooplocal-update.component.html',
})
export class CooplocalUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [null, [Validators.required]],
  });

  constructor(protected cooplocalService: CooplocalService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cooplocal }) => {
      this.updateForm(cooplocal);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cooplocal = this.createFromForm();
    if (cooplocal.id !== undefined) {
      this.subscribeToSaveResponse(this.cooplocalService.update(cooplocal));
    } else {
      this.subscribeToSaveResponse(this.cooplocalService.create(cooplocal));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICooplocal>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(cooplocal: ICooplocal): void {
    this.editForm.patchValue({
      id: cooplocal.id,
    });
  }

  protected createFromForm(): ICooplocal {
    return {
      ...new Cooplocal(),
      id: this.editForm.get(['id'])!.value,
    };
  }
}
