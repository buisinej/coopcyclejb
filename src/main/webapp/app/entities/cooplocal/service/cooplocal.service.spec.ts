import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICooplocal, Cooplocal } from '../cooplocal.model';

import { CooplocalService } from './cooplocal.service';

describe('Service Tests', () => {
  describe('Cooplocal Service', () => {
    let service: CooplocalService;
    let httpMock: HttpTestingController;
    let elemDefault: ICooplocal;
    let expectedResult: ICooplocal | ICooplocal[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(CooplocalService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Cooplocal', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new Cooplocal()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Cooplocal', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Cooplocal', () => {
        const patchObject = Object.assign({}, new Cooplocal());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Cooplocal', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Cooplocal', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addCooplocalToCollectionIfMissing', () => {
        it('should add a Cooplocal to an empty array', () => {
          const cooplocal: ICooplocal = { id: 'ABC' };
          expectedResult = service.addCooplocalToCollectionIfMissing([], cooplocal);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(cooplocal);
        });

        it('should not add a Cooplocal to an array that contains it', () => {
          const cooplocal: ICooplocal = { id: 'ABC' };
          const cooplocalCollection: ICooplocal[] = [
            {
              ...cooplocal,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addCooplocalToCollectionIfMissing(cooplocalCollection, cooplocal);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Cooplocal to an array that doesn't contain it", () => {
          const cooplocal: ICooplocal = { id: 'ABC' };
          const cooplocalCollection: ICooplocal[] = [{ id: 'CBA' }];
          expectedResult = service.addCooplocalToCollectionIfMissing(cooplocalCollection, cooplocal);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(cooplocal);
        });

        it('should add only unique Cooplocal to an array', () => {
          const cooplocalArray: ICooplocal[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'Unbranded' }];
          const cooplocalCollection: ICooplocal[] = [{ id: 'ABC' }];
          expectedResult = service.addCooplocalToCollectionIfMissing(cooplocalCollection, ...cooplocalArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const cooplocal: ICooplocal = { id: 'ABC' };
          const cooplocal2: ICooplocal = { id: 'CBA' };
          expectedResult = service.addCooplocalToCollectionIfMissing([], cooplocal, cooplocal2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(cooplocal);
          expect(expectedResult).toContain(cooplocal2);
        });

        it('should accept null and undefined values', () => {
          const cooplocal: ICooplocal = { id: 'ABC' };
          expectedResult = service.addCooplocalToCollectionIfMissing([], null, cooplocal, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(cooplocal);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
