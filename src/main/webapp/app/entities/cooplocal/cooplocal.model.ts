import { ICoopnational } from 'app/entities/coopnational/coopnational.model';
import { ILivreur } from 'app/entities/livreur/livreur.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';

export interface ICooplocal {
  id?: string;
  coopnationals?: ICoopnational[] | null;
  livreurs?: ILivreur[] | null;
  restaurants?: IRestaurant[] | null;
}

export class Cooplocal implements ICooplocal {
  constructor(
    public id?: string,
    public coopnationals?: ICoopnational[] | null,
    public livreurs?: ILivreur[] | null,
    public restaurants?: IRestaurant[] | null
  ) {}
}

export function getCooplocalIdentifier(cooplocal: ICooplocal): string | undefined {
  return cooplocal.id;
}
