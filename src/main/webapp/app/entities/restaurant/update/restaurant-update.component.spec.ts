jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { RestaurantService } from '../service/restaurant.service';
import { IRestaurant, Restaurant } from '../restaurant.model';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';
import { CooplocalService } from 'app/entities/cooplocal/service/cooplocal.service';

import { RestaurantUpdateComponent } from './restaurant-update.component';

describe('Component Tests', () => {
  describe('Restaurant Management Update Component', () => {
    let comp: RestaurantUpdateComponent;
    let fixture: ComponentFixture<RestaurantUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let restaurantService: RestaurantService;
    let cooplocalService: CooplocalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [RestaurantUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(RestaurantUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RestaurantUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      restaurantService = TestBed.inject(RestaurantService);
      cooplocalService = TestBed.inject(CooplocalService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Cooplocal query and add missing value', () => {
        const restaurant: IRestaurant = { id: 456 };
        const cooplocals: ICooplocal[] = [{ id: 'a c c' }];
        restaurant.cooplocals = cooplocals;

        const cooplocalCollection: ICooplocal[] = [{ id: 'bandwidth Loan THX' }];
        spyOn(cooplocalService, 'query').and.returnValue(of(new HttpResponse({ body: cooplocalCollection })));
        const additionalCooplocals = [...cooplocals];
        const expectedCollection: ICooplocal[] = [...additionalCooplocals, ...cooplocalCollection];
        spyOn(cooplocalService, 'addCooplocalToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ restaurant });
        comp.ngOnInit();

        expect(cooplocalService.query).toHaveBeenCalled();
        expect(cooplocalService.addCooplocalToCollectionIfMissing).toHaveBeenCalledWith(cooplocalCollection, ...additionalCooplocals);
        expect(comp.cooplocalsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const restaurant: IRestaurant = { id: 456 };
        const cooplocals: ICooplocal = { id: 'Market radical' };
        restaurant.cooplocals = [cooplocals];

        activatedRoute.data = of({ restaurant });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(restaurant));
        expect(comp.cooplocalsSharedCollection).toContain(cooplocals);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const restaurant = { id: 123 };
        spyOn(restaurantService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ restaurant });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: restaurant }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(restaurantService.update).toHaveBeenCalledWith(restaurant);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const restaurant = new Restaurant();
        spyOn(restaurantService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ restaurant });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: restaurant }));
        saveSubject.complete();

        // THEN
        expect(restaurantService.create).toHaveBeenCalledWith(restaurant);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const restaurant = { id: 123 };
        spyOn(restaurantService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ restaurant });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(restaurantService.update).toHaveBeenCalledWith(restaurant);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCooplocalById', () => {
        it('Should return tracked Cooplocal primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackCooplocalById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });

    describe('Getting selected relationships', () => {
      describe('getSelectedCooplocal', () => {
        it('Should return option if no Cooplocal is selected', () => {
          const option = { id: 'ABC' };
          const result = comp.getSelectedCooplocal(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Cooplocal for according option', () => {
          const option = { id: 'ABC' };
          const selected = { id: 'ABC' };
          const selected2 = { id: 'CBA' };
          const result = comp.getSelectedCooplocal(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Cooplocal is not selected', () => {
          const option = { id: 'ABC' };
          const selected = { id: 'CBA' };
          const result = comp.getSelectedCooplocal(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });
    });
  });
});
