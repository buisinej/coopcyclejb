import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { CoopnationalComponent } from './list/coopnational.component';
import { CoopnationalDetailComponent } from './detail/coopnational-detail.component';
import { CoopnationalUpdateComponent } from './update/coopnational-update.component';
import { CoopnationalDeleteDialogComponent } from './delete/coopnational-delete-dialog.component';
import { CoopnationalRoutingModule } from './route/coopnational-routing.module';

@NgModule({
  imports: [SharedModule, CoopnationalRoutingModule],
  declarations: [CoopnationalComponent, CoopnationalDetailComponent, CoopnationalUpdateComponent, CoopnationalDeleteDialogComponent],
  entryComponents: [CoopnationalDeleteDialogComponent],
})
export class CoopnationalModule {}
