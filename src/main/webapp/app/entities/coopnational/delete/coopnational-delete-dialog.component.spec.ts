jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { CoopnationalService } from '../service/coopnational.service';

import { CoopnationalDeleteDialogComponent } from './coopnational-delete-dialog.component';

describe('Component Tests', () => {
  describe('Coopnational Management Delete Component', () => {
    let comp: CoopnationalDeleteDialogComponent;
    let fixture: ComponentFixture<CoopnationalDeleteDialogComponent>;
    let service: CoopnationalService;
    let mockActiveModal: NgbActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CoopnationalDeleteDialogComponent],
        providers: [NgbActiveModal],
      })
        .overrideTemplate(CoopnationalDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CoopnationalDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(CoopnationalService);
      mockActiveModal = TestBed.inject(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete('ABC');
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith('ABC');
          expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.close).not.toHaveBeenCalled();
        expect(mockActiveModal.dismiss).toHaveBeenCalled();
      });
    });
  });
});
