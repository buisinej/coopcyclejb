jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CoopnationalService } from '../service/coopnational.service';
import { ICoopnational, Coopnational } from '../coopnational.model';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';
import { CooplocalService } from 'app/entities/cooplocal/service/cooplocal.service';

import { CoopnationalUpdateComponent } from './coopnational-update.component';

describe('Component Tests', () => {
  describe('Coopnational Management Update Component', () => {
    let comp: CoopnationalUpdateComponent;
    let fixture: ComponentFixture<CoopnationalUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let coopnationalService: CoopnationalService;
    let cooplocalService: CooplocalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CoopnationalUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CoopnationalUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CoopnationalUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      coopnationalService = TestBed.inject(CoopnationalService);
      cooplocalService = TestBed.inject(CooplocalService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Cooplocal query and add missing value', () => {
        const coopnational: ICoopnational = { id: 'CBA' };
        const cooplocal: ICooplocal = { id: 'Designer Bedfordshire' };
        coopnational.cooplocal = cooplocal;

        const cooplocalCollection: ICooplocal[] = [{ id: 'Birmanie c back-end' }];
        spyOn(cooplocalService, 'query').and.returnValue(of(new HttpResponse({ body: cooplocalCollection })));
        const additionalCooplocals = [cooplocal];
        const expectedCollection: ICooplocal[] = [...additionalCooplocals, ...cooplocalCollection];
        spyOn(cooplocalService, 'addCooplocalToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ coopnational });
        comp.ngOnInit();

        expect(cooplocalService.query).toHaveBeenCalled();
        expect(cooplocalService.addCooplocalToCollectionIfMissing).toHaveBeenCalledWith(cooplocalCollection, ...additionalCooplocals);
        expect(comp.cooplocalsSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const coopnational: ICoopnational = { id: 'CBA' };
        const cooplocal: ICooplocal = { id: 'Rand' };
        coopnational.cooplocal = cooplocal;

        activatedRoute.data = of({ coopnational });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(coopnational));
        expect(comp.cooplocalsSharedCollection).toContain(cooplocal);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coopnational = { id: 'ABC' };
        spyOn(coopnationalService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coopnational });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: coopnational }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(coopnationalService.update).toHaveBeenCalledWith(coopnational);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coopnational = new Coopnational();
        spyOn(coopnationalService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coopnational });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: coopnational }));
        saveSubject.complete();

        // THEN
        expect(coopnationalService.create).toHaveBeenCalledWith(coopnational);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const coopnational = { id: 'ABC' };
        spyOn(coopnationalService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ coopnational });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(coopnationalService.update).toHaveBeenCalledWith(coopnational);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackCooplocalById', () => {
        it('Should return tracked Cooplocal primary key', () => {
          const entity = { id: 'ABC' };
          const trackResult = comp.trackCooplocalById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
