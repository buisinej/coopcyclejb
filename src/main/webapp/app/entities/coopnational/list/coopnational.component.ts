import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoopnational } from '../coopnational.model';
import { CoopnationalService } from '../service/coopnational.service';
import { CoopnationalDeleteDialogComponent } from '../delete/coopnational-delete-dialog.component';

@Component({
  selector: 'jhi-coopnational',
  templateUrl: './coopnational.component.html',
})
export class CoopnationalComponent implements OnInit {
  coopnationals?: ICoopnational[];
  isLoading = false;

  constructor(protected coopnationalService: CoopnationalService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.coopnationalService.query().subscribe(
      (res: HttpResponse<ICoopnational[]>) => {
        this.isLoading = false;
        this.coopnationals = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: ICoopnational): string {
    return item.id!;
  }

  delete(coopnational: ICoopnational): void {
    const modalRef = this.modalService.open(CoopnationalDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coopnational = coopnational;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
