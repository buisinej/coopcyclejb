import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { CoopnationalService } from '../service/coopnational.service';

import { CoopnationalComponent } from './coopnational.component';

describe('Component Tests', () => {
  describe('Coopnational Management Component', () => {
    let comp: CoopnationalComponent;
    let fixture: ComponentFixture<CoopnationalComponent>;
    let service: CoopnationalService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CoopnationalComponent],
      })
        .overrideTemplate(CoopnationalComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CoopnationalComponent);
      comp = fixture.componentInstance;
      service = TestBed.inject(CoopnationalService);

      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [{ id: 'ABC' }],
            headers,
          })
        )
      );
    });

    it('Should call load all on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.coopnationals?.[0]).toEqual(jasmine.objectContaining({ id: 'ABC' }));
    });
  });
});
