import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoopnationalComponent } from '../list/coopnational.component';
import { CoopnationalDetailComponent } from '../detail/coopnational-detail.component';
import { CoopnationalUpdateComponent } from '../update/coopnational-update.component';
import { CoopnationalRoutingResolveService } from './coopnational-routing-resolve.service';

const coopnationalRoute: Routes = [
  {
    path: '',
    component: CoopnationalComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoopnationalDetailComponent,
    resolve: {
      coopnational: CoopnationalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoopnationalUpdateComponent,
    resolve: {
      coopnational: CoopnationalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoopnationalUpdateComponent,
    resolve: {
      coopnational: CoopnationalRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coopnationalRoute)],
  exports: [RouterModule],
})
export class CoopnationalRoutingModule {}
