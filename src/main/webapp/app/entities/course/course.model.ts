import { ICommande } from 'app/entities/commande/commande.model';
import { ILivreur } from 'app/entities/livreur/livreur.model';

export interface ICourse {
  id?: number;
  prix?: number;
  distance?: number;
  commandes?: ICommande[] | null;
  livreur?: ILivreur | null;
}

export class Course implements ICourse {
  constructor(
    public id?: number,
    public prix?: number,
    public distance?: number,
    public commandes?: ICommande[] | null,
    public livreur?: ILivreur | null
  ) {}
}

export function getCourseIdentifier(course: ICourse): number | undefined {
  return course.id;
}
