import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'client',
        data: { pageTitle: 'myblogApp.client.home.title' },
        loadChildren: () => import('./client/client.module').then(m => m.ClientModule),
      },
      {
        path: 'commande',
        data: { pageTitle: 'myblogApp.commande.home.title' },
        loadChildren: () => import('./commande/commande.module').then(m => m.CommandeModule),
      },
      {
        path: 'course',
        data: { pageTitle: 'myblogApp.course.home.title' },
        loadChildren: () => import('./course/course.module').then(m => m.CourseModule),
      },
      {
        path: 'livreur',
        data: { pageTitle: 'myblogApp.livreur.home.title' },
        loadChildren: () => import('./livreur/livreur.module').then(m => m.LivreurModule),
      },
      {
        path: 'plat',
        data: { pageTitle: 'myblogApp.plat.home.title' },
        loadChildren: () => import('./plat/plat.module').then(m => m.PlatModule),
      },
      {
        path: 'restaurant',
        data: { pageTitle: 'myblogApp.restaurant.home.title' },
        loadChildren: () => import('./restaurant/restaurant.module').then(m => m.RestaurantModule),
      },
      {
        path: 'cooplocal',
        data: { pageTitle: 'myblogApp.cooplocal.home.title' },
        loadChildren: () => import('./cooplocal/cooplocal.module').then(m => m.CooplocalModule),
      },
      {
        path: 'coopnational',
        data: { pageTitle: 'myblogApp.coopnational.home.title' },
        loadChildren: () => import('./coopnational/coopnational.module').then(m => m.CoopnationalModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
