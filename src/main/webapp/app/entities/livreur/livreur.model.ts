import { ICourse } from 'app/entities/course/course.model';
import { ICooplocal } from 'app/entities/cooplocal/cooplocal.model';

export interface ILivreur {
  id?: number;
  name?: string;
  courses?: ICourse[] | null;
  cooplocal?: ICooplocal | null;
}

export class Livreur implements ILivreur {
  constructor(public id?: number, public name?: string, public courses?: ICourse[] | null, public cooplocal?: ICooplocal | null) {}
}

export function getLivreurIdentifier(livreur: ILivreur): number | undefined {
  return livreur.id;
}
