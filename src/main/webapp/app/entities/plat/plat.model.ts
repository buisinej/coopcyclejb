import { ICommande } from 'app/entities/commande/commande.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';

export interface IPlat {
  id?: number;
  name?: string;
  prix?: number;
  commandes?: ICommande[] | null;
  restaurant?: IRestaurant | null;
}

export class Plat implements IPlat {
  constructor(
    public id?: number,
    public name?: string,
    public prix?: number,
    public commandes?: ICommande[] | null,
    public restaurant?: IRestaurant | null
  ) {}
}

export function getPlatIdentifier(plat: IPlat): number | undefined {
  return plat.id;
}
